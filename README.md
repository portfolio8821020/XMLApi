# XMLApi

### Open API Definition
* http://localhost:8080/v3/api-docs
* http://localhost:8080/swagger-ui/index.html

### Requirements
* JDK 17

### Get Started
* install jdk 17
* clone the repository https://gitlab.com/portfolio8821020/XMLApi.git
* run with maven or java

### Docker Image
* registry.gitlab.com/portfolio8821020/xmlapi:main

### Run Image
* docker pull registry.gitlab.com/portfolio8821020/xmlapi:main
* docker run -d -p 8080:8080 registry.gitlab.com/portfolio8821020/xmlapi:main

### Postman Collection
* XMLApi.postman_collection.json
